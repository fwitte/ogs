+++
title = "Publications"

[menu.main]
weight = 3

+++

<h1 class="sm:text-3xl text-2xl font-medium title-font mb-2 text-gray-900">OpenGeoSys publications overview</h1>
<p class="lg:w-1/2 w-full leading-relaxed text-base">If you find OpenGeoSys useful for your research please cite us!</p>

### Cite the software

```bibtex
@software{ogs:6.5.1,
  author       = {Bilke, Lars and
                  Naumov, Dmitri and
                  Wang, Wenqing and
                  Fischer, Thomas and
                  Lehmann, Christoph and
                  Buchwald, Jörg and
                  Shao, Haibing and
                  Kiszkurno, Feliks K. and
                  Chen, Chaofan and
                  Silbermann, Christian and
                  Radeisen, Eike and
                  Zill, Florian and
                  Frieder, Loer and
                  Kessler, Kristof and
                  Ghasabeh, Mehran},
  title        = {OpenGeoSys},
  month        = mar,
  year         = 2024,
  publisher    = {Zenodo},
  version      = {6.5.1},
  doi          = {10.5281/zenodo.10890088},
  url          = {https://doi.org/10.5281/zenodo.10890088}
}
```

### Highlighted paper

<div class="bg-brand-50 rounded-lg shadow p-2 mb-4 text-gray-900">
{{< bib "kolditz2012" >}}
</div>

<div class="bg-brand-50 rounded-lg shadow p-2 mb-4 text-gray-900">
{{< bib "bilke2019" >}}
</div>

<div class="bg-brand-50 rounded-lg shadow p-2 mb-4 text-gray-900">
{{< bib "lu2022" >}}
</div>

### More publications
