###
# Copyright (c) 2012-2024, OpenGeoSys Community (http://www.opengeosys.org)
# Distributed under a Modified BSD License.
# See accompanying file LICENSE.txt or
# http://www.opengeosys.org/project/license
###

# Execute this file to generate TESPy network csv files
import numpy as np
from tespy.components import (
    Merge,
    Pump,
    SimpleHeatExchanger,
    Sink,
    Source,
    Splitter,
)
from tespy.connections import Bus, Connection, Ref
from tespy.networks import Network
from tespy.tools.characteristics import CharLine

# %% network
btes = Network(
    T_unit="K",
    p_unit="bar",
    h_unit="kJ / kg",
)

# %% components
fc_in = Source("from consumer inflow")
fc_out = Sink("from consumer outflow")

pu = Pump("pump")

sp = Splitter("splitter", num_out=3)

# bhe:
bhe_name = "BHE1"
assert "BHE1" in bhe_name, "BHE should be named with 'BHE1'"
bhe1 = SimpleHeatExchanger(bhe_name)
bhe_name = "BHE2"
assert "BHE2" in bhe_name, "BHE should be named with 'BHE2'"
bhe2 = SimpleHeatExchanger(bhe_name)
bhe_name = "BHE3"
assert "BHE3" in bhe_name, "BHE should be named with 'BHE3'"
bhe3 = SimpleHeatExchanger(bhe_name)

mg = Merge("merge", num_in=3)

cons = SimpleHeatExchanger("consumer")

# %% connections
fc_pu = Connection(fc_in, "out1", pu, "in1")

pu_sp = Connection(pu, "out1", sp, "in1")

sp_bhe1 = Connection(sp, "out1", bhe1, "in1")
sp_bhe2 = Connection(sp, "out2", bhe2, "in1")
sp_bhe3 = Connection(sp, "out3", bhe3, "in1")

bhe1_mg = Connection(bhe1, "out1", mg, "in1")
bhe2_mg = Connection(bhe2, "out1", mg, "in2")
bhe3_mg = Connection(bhe3, "out1", mg, "in3")

mg_cons = Connection(mg, "out1", cons, "in1")

cons_fc = Connection(cons, "out1", fc_out, "in1")

btes.add_conns(
    fc_pu, pu_sp, sp_bhe1, sp_bhe2, sp_bhe3, bhe1_mg, bhe2_mg, bhe3_mg, mg_cons, cons_fc
)

[c.set_attr(h0=50, p0=2) for c in btes.conns["object"]]


# %% paramerization
## components paramerization
# pump
# flow_char
# provide volumetric flow in m^3 / s
x = np.array(
    [
        0.00,
        0.00001952885971862,
        0.00390577194372,
        0.005858657915586,
        0.007811543887448,
        0.00976442985931,
        0.011717315831173,
        0.013670201803035,
        0.015623087774897,
        0.017575973746759,
        0.019528859718621,
        0.021481745690483,
        0.023434631662345,
        0.025387517634207,
        0.027340403606069,
        0.029293289577931,
        0.031246175549793,
        0.033199061521655,
        0.035151947493517,
        0.037104833465379,
        0.039057719437241,
        0.041010605409104,
        0.042963491380966,
        0.044916377352828,
        0.04686926332469,
        0.048822149296552,
        0.050775035268414,
        0.052727921240276,
        0.054680807212138,
        0.056633693184,
    ]
)

# provide head in Pa
y = (
    np.array(
        [
            0.47782539,
            0.47725723,
            0.47555274,
            0.47271192,
            0.46873478,
            0.46362130,
            0.45737151,
            0.44998538,
            0.44146293,
            0.43180416,
            0.4220905,
            0.40907762,
            0.39600986,
            0.38180578,
            0.36646537,
            0.34998863,
            0.33237557,
            0.31362618,
            0.29374046,
            0.27271841,
            0.25056004,
            0.22726535,
            0.20283432,
            0.17726697,
            0.15056329,
            0.12272329,
            0.09374696,
            0.06363430,
            0.03238531,
            0.00000000,
        ]
    )
    * 1e5
)

char = CharLine(x=x, y=y)
pu.set_attr(flow_char={"char_func": char, "is_set": True})
pu.set_attr(eta_s=0.90)

# bhes
bhe1.set_attr(D=0.013665, L=100, ks=0.00001)
bhe2.set_attr(D=0.013665, L=100, ks=0.00001)
bhe3.set_attr(D=0.013665, L=100, ks=0.00001)

# consumer
cons.set_attr(D=0.2, L=20, ks=0.00001)
# busses
heat = Bus("consumer heat demand")
heat.add_comps({"comp": cons, "param": "P"})
btes.add_busses(heat)
# consumer heat demand
heat.set_attr(P=-3000)  # W


## connection parametrization
# system inlet
inflow_head = 2  # bar

fc_pu.set_attr(p=inflow_head, m=0.6, fluid={"Water": 1})

# for BHEs:
# Tout:
bhe1_mg.set_attr(T=303.15)
bhe2_mg.set_attr(T=303.15)
bhe3_mg.set_attr(T=303.15)

# imposed boundary condition: ensure all heat from BHEs are consumed on 'consumer'
pu_sp.set_attr(h=Ref(cons_fc, 1, 0))

# %% solve
btes.solve("design")
# btes.print_results()

# %% save to csv (results only):
btes.save("bhe_tespy_07x")
# %% save to json structure (complete model, that can be reimported)
btes.export("tespy_nw")
